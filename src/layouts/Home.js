import React from 'react'
import Header from '../components/organisms/Header';
import List from '../components/organisms/List';

const Home = () => {
  return (
    <React.Fragment>
      <Header/>
      <List/>
    </React.Fragment>
  )
};

export default Home;
