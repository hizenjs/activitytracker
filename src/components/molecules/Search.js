import React from 'react'
import styled from 'styled-components'

import Icon from '../atoms/Icon';
import Avatar from '../atoms/Avatar';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
`

const Search = () => {
  return (
    <Wrapper>
        <Avatar source="https://i.imgur.com/pX1op9a.png" medium/>
      <Icon name="search" size={18} color="#78899A"/>
    </Wrapper>
  )
}

export default Search
