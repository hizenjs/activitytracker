import React, { useState } from 'react'
import styled from 'styled-components'
import CheckBox from '@react-native-community/checkbox';

import Title from '../atoms/Title';
import Subtitle from '../atoms/Subtitle';
import Spacer from '../atoms/Spacer';
import Avatar from '../atoms/Avatar';

const Wrapper = styled.View`
  margin-top: 24px;
  margin-left: 22px;
  margin-right: 22px;
  border-radius: 12px;
  background-color: #ffffff;
  padding: 14px 12px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  shadow-color: #000000;
  shadow-offset: 0 3px;
  shadow-opacity: 0.18;
  shadow-radius: 6px;
  elevation: 4;
`

const Container = styled.View`
  max-width: 240px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`

const Content = styled.View`
  padding: 8px 12px;
`
const Concatainer = styled.View`
  display: flex;
  flex-direction: row;
`

const Declaration = styled.View`
  width: 4px;
  height: 100%;
  border-radius: 4px;
  position: absolute;
  background-color: #FFA74E;
`

const colors = ['#FFA74E', '#CB52E8', '#56f5a8', '#e851bd', '#ed3e3e', '#3c9cf0', '#f0c83a']

const Card = ({type}) => {
  const [isSelected, setSelection] = useState(false);

  const checkbox = {
    alignSelf: "center",
    borderColor: "#EBF4FE",
    borderRadius: 6,
    width: 36,
    height: 36
  }

  const colorSelector = () => {
    return colors[Math.floor(Math.random() * Math.floor(7))]
  }

  const renderSimple = () => (
    <Wrapper>
      <Container>
        <Declaration style={{backgroundColor : isSelected ? '#EBF4FE' : colorSelector()}}/>
        <Content>
          <Title value="Morning warmup with Elise" h4 bold />
        </Content>
      </Container>
      <CheckBox
        value={isSelected}
        onValueChange={setSelection}
        tintColor="#EBF4FE"
        onCheckColor="#3494F4"
        tintColors={{true: '#3494F4', false: '#EBF4FE'}}
      />
    </Wrapper>
  )

  const renderMate = () => (
    <Wrapper>
      <Container>
        <Declaration style={{backgroundColor : isSelected ? '#EBF4FE' : colorSelector()}}/>
        <Content>
          <Title value="Gym workout" h4 bold />
          <Spacer size={10} vertical />
          <Subtitle value="Mates:" h4 />
          <Concatainer>
            <Avatar source="https://i.imgur.com/UTKVYOT.png" small bordered />
            <Avatar source="https://i.imgur.com/8lwxriX.png" small bordered customStyle={{marginLeft: -8}} />
            <Avatar source="https://i.imgur.com/vJFkpQu.png" small bordered customStyle={{marginLeft: -8}} />
          </Concatainer>
        </Content>
      </Container>
      <CheckBox
        value={isSelected}
        onValueChange={setSelection}
        tintColor="#EBF4FE"
        onTintColor="#3494F4"
        onCheckColor="#3494F4"
        tintColors={{true: '#3494F4', false: '#EBF4FE'}}
      />
    </Wrapper>
  )

  const renderTime = () => (
    <Wrapper>
      <Container>
        <Declaration style={{backgroundColor : isSelected ? '#EBF4FE' : colorSelector()}}/>
        <Content>
          <Title value="Walk the dog" h4 bold />
          <Spacer size={10} vertical />
          <Subtitle value="Time:" h4 />
          <Title value="15:00 - 15:30" h5 />
        </Content>
      </Container>
      <CheckBox
        value={isSelected}
        onValueChange={setSelection}
        tintColor="#EBF4FE"
        onTintColor="#3494F4"
        onCheckColor="#3494F4"
        tintColors={{true: '#3494F4', false: '#EBF4FE'}}
      />
    </Wrapper>
  )

  const render = (type) => {
    if (type === 'mate') return renderMate()
    if (type === 'time') return renderTime()
    return renderSimple()
  }

  return render(type)
}

export default Card
