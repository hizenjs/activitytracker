import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import Icon from '../atoms/Icon';
import Title from '../atoms/Title';
import Subtitle from '../atoms/Subtitle';
import Progress from '../atoms/Progress';

const Wrapper = styled.View`
  margin-top: 8px;
  width: 100%
`

const Container = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  margin-bottom: 8px;
`

const SubContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
`

const Content = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-right: 6px;
`

const Tracker = () => {
  const [value, setValue] = useState(40)

  useEffect(() => {
    const interval = setInterval(() => {
      setValue(Math.floor(Math.random() * Math.floor(100)));
    }, 5000);
    return () => clearInterval(interval);
  }, []);

  return (
    <Wrapper>
      <Container>
        <SubContainer>
          <Content>
            <Subtitle h4 value="Your progress"/>
            <Title h2 value="Today" bold/>
          </Content>
          <Icon name="edit" size={19} color="#78899A"/>
        </SubContainer>
        <Subtitle h3 value="4/12" color="#3494F4" align="right" bold/>
      </Container>
      <Progress value={value} />
    </Wrapper>
  )
}

export default Tracker
