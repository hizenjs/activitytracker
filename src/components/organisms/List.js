import React from 'react'
import { FlatList } from 'react-native';
import Card from '../molecules/Card';

const data = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    type: 'time'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bb',
    type: 'mate'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bc',
    type: 'simple'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bd',
    type: 'mate'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28be',
    type: 'time'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bf',
    type: 'mate'
  }
]


const List = () => {

  const renderItem = ({ item }) => (
    <Card key={item.id} type={item.type}/>
  );

  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      style={{backgroundColor: '#F5FAFE', paddingBottom: 22}}
    />
  )
}

export default List
