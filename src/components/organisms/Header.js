import React from 'react'
import { Platform } from 'react-native'
import styled from 'styled-components'

import Tracker from '../molecules/Tracker';
import Search from '../molecules/Search';

const Wrapper  = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  background: #ffffff;
  padding: 18px 22px;
  border-bottom-left-radius: 22px;
  border-bottom-right-radius: 22px;
  margin-top: 44px;
  shadow-color: #000000;
  shadow-offset: 0 6px;
  shadow-opacity: 0.1;
  shadow-radius: 6px;
  elevation: 10;
`

const Header = () => {
  return (
    <Wrapper style={{marginTop: Platform.OS === 'ios' ? 44 : 0}}>
      <Search/>
      <Tracker/>
    </Wrapper>
  )
}

export default Header
