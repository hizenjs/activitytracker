import React from 'react'
import styled from 'styled-components'

const Text = styled.Text``

const Title = (props) => {
  const {value, align, h1, h2, h3, h4, h5, bold, color} = props

  const customSize = () => {
    return h1 ? 34 : h2 ? 24 : h3 ? 18 : h4 ? 15 : h5 ? 11 : 18
  }

  const aligner = () => {
    if (align !== 'center' || align !== 'left' || align !== 'right')
      return 'left'
    return align
  }

  const customStyle = {
    textAlign: aligner(),
    fontSize: customSize(),
    fontWeight: bold ? 'bold' : 'normal',
    color: color || '#153250'
  };

  return <Text style={customStyle}>{value}</Text>
};

export default Title
