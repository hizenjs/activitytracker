import React from 'react'
import styled from 'styled-components'

const Text = styled.Text``

const Subtitle = (props) => {
  const {value, align, h1, h2, h3, h4, h5, bold, color} = props

  const customSize = () => {
    return h1 ? 24 : h2 ? 18 : h3 ? 15 : h4 ? 11 : h5 ? 9 : 16
  }

  const aligner = () => {
    if (align !== 'center' || align !== 'left' || align !== 'right')
      return 'left'
    return align
  }

  const customStyle = {
    textAlign: aligner(),
    fontSize: customSize(),
    fontWeight: bold ? 'bold' : 'normal',
    opacity: h1 ? 1 : h2 ? 1 : h3 ? 1 : .58,
    color: color || '#153250'
  };

  return <Text style={customStyle}>{value}</Text>
};

export default Subtitle
