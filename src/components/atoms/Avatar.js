import React from 'react'
import styled from 'styled-components'

const Image = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 40px;
  border-width: 1px;
  border-color: #ffffff;
`

const Avatar = (props) => {
  const {source, bordered, large, medium, small, customStyle} = props
  const uri = {uri: source}

  const customWidth = () => {
    return large ? 60 : medium ? 40 : small ? 22 : 40
  }

  const customHeight = () => {
    return large ? 60 : medium ? 40 : small ? 22 : 40
  }

  const style = {
    ...customStyle,
    borderWidth: bordered ? 1 : 0,
    width: customWidth(),
    height: customHeight()
  };

  return <Image source={uri} style={style} />
};

export default Avatar
