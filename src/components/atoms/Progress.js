import React from 'react'
import { Animated } from 'react-native';
import styled from 'styled-components'

const Outer = styled.View`
  background-color: #EBF4FE;
  border-radius: 7px;
  height: 7px;
  width: 100%;
`
const Inner = styled.View`
  background-color: #3494F4;
  border-radius: 7px;
  height: 7px;
  width: 0;
`

const InnerAnimated = Animated.createAnimatedComponent(Inner);

const Progress = ({value}) => {

  const formatValue = () => {
    if (typeof value !== 'number') return '0%'
    else if (value > 100) return '100%'
    return value + '%'
  }

  return (
    <Outer>
      <InnerAnimated style={{width: formatValue()}}/>
    </Outer>
  )
}

export default Progress
