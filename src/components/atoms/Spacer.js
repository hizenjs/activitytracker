import React from 'react'
import styled from 'styled-components'

const View = styled.View``

const Spacer = ({size, horizontal, vertical}) => {
  const space =  {
    width: horizontal && size,
    height: vertical  && size
  }
  return <View style={space}/>
};

export default Spacer
