import React from 'react'
import styled from 'styled-components'
import Icons from 'react-native-vector-icons/Feather';

const Wrapper = styled.View`
  border-radius: 4px;
`

const Icon = ({active, size, name, color}) => {

  const formatSize = () => {
    if (typeof size !== 'number') return 16
    return size
  }

  const customStyle = {
    width: formatSize(),
    height: size,
    opacity: active ? 1 : .4
  }

  return (
    <Wrapper style={customStyle}>
      <Icons name={name} size={size} color={color} />
    </Wrapper>
  )
}

export default Icon
