import React from 'react';
import {StatusBar} from 'react-native';
import Home from './src/layouts/Home';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />
      <Home/>
    </>
  );
};

export default App;
